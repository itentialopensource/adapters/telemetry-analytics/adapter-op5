
## 0.1.4 [09-26-2022]

* Bug fixes and performance improvements

See commit 8785b64

---

## 0.1.3 [09-26-2022]

* Bug fixes and performance improvements

See commit 1083969

---

## 0.1.2 [09-26-2022]

* Bug fixes and performance improvements

See commit cad8695

---

## 0.1.1 [09-26-2022]

* Bug fixes and performance improvements

See commit 2e35187

---
