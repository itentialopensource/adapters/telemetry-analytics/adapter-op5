# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the op5 System. The API that was used to build the adapter for op5 is usually available in the report directory of this adapter. The adapter utilizes the op5 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The OP5 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with OP5.With this adapter you have the ability to perform operations with OP5 on items such as:

- Filter
- Config
- Nachos

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
