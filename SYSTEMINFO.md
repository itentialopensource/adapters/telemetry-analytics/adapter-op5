# OP5

Vendor: ITRS
Homepage: https://www.itrsgroup.com/

Product: OP5
Product Page: https://www.itrsgroup.com/products/network-monitoring-op5-monitor

## Introduction
We classify OP5 into the Service Assurance domain as OP5 provides real-time insights and control over IT infrastructure performance and availability.

## Why Integrate
The OP5 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with OP5.With this adapter you have the ability to perform operations with OP5 on items such as:

- Filter
- Config
- Nachos

## Additional Product Documentation
The [API documents for OP5](https://docs.itrsgroup.com/docs/op5-monitor/8.1.0/topics/api/monitor-api.html)
