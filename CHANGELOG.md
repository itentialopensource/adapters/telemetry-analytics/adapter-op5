
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:52PM

See merge request itentialopensource/adapters/adapter-op5!11

---

## 0.3.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-op5!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:01PM

See merge request itentialopensource/adapters/adapter-op5!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:14PM

See merge request itentialopensource/adapters/adapter-op5!7

---

## 0.3.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!6

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:10PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!5

---

## 0.2.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!4

---

## 0.2.2 [03-12-2024]

* Changes made at 2024.03.12_10:56AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!3

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:30AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!2

---

## 0.2.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-op5!1

---

## 0.1.4 [09-26-2022]

* Bug fixes and performance improvements

See commit 8785b64

---

## 0.1.3 [09-26-2022]

* Bug fixes and performance improvements

See commit 1083969

---

## 0.1.2 [09-26-2022]

* Bug fixes and performance improvements

See commit cad8695

---

## 0.1.1 [09-26-2022]

* Bug fixes and performance improvements

See commit 2e35187

---
